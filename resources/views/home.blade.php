@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @guest
                    <div class="d-flex justify-content-center">
                        <h5>Vær venlig at logge ind her ved dette <a href="/login">link</a></h5>
                    </div>
                @else
                    <div class="card">
                        <div class="card-header">Oversigt</div>
                        <div class="card-body">
                            <h1 class="text-center">Hejsa {{Auth::user()->name}}</h1>
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <a href="/todos">Se Todo Liste</a>
                        </div>
                    </div>
                @endguest
            </div>
        </div>
    </div>
@endsection
