@extends('layouts.app')

@section('content')
    <div class="container d-flex justify-content-center align-items-center flex-column">
        <div class="container">
            <form action="/todos/{{$todo->id}}" method="POST">
                @method('PATCH')
                @csrf


                <div class="form-group">
                    <label for="name">Navn</label>
                    <input type="text" name="name" class="form-control" value="{{$todo->name}}">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success">Opdater Todo</button>
                </div>
            </form>
        </div>

        <div class="container">
            <form action="/todos/{{$todo->id}}" method="post">
                @method('DELETE')
                @csrf

                <div class="form-group">
                    <button type="submit" class="btn btn-danger">Slet Todo</button>
                </div>
            </form>
        </div>


    </div>

@endsection
