@extends('layouts.app')

@section('content')
    <div class="container d-flex justify-content-center">
        <form class="col-sm-3" action="/todos" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label for="name">Navn</label>
                <input type="text" name="name" class="form-control">
            </div>
            <div class="form-group">
                <button class="btn btn-primary" type="submit">Opret ny todo</button>
            </div>
        </form>
    </div>
@endsection
