@extends('layouts.app')

@section('content')
    <div class="container d-flex justify-content-center align-items-center flex-column">
        <h1 class="text-center">Todo Liste</h1>
        <a class="d-flex justify-content-center" style="margin-bottom: 25px;" href="/todos/create">Lav en ny todo</a>
        @foreach($todos as $todo)
                <div class="card" style="width: 18rem; margin-bottom: 15px;">
                    <div class="card-body">
                        <h5 class="card-title">{{ $todo->name }}</h5>
                        <a href="/todos/{{$todo->id}}/edit">
                            <button class="btn btn-primary">
                                Rediger
                            </button>
                        </a>
                    </div>
                </div>
        @endforeach
    </div>
@endsection
